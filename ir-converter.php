<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<div class="form-horizontal">
    <div class="form-group">
        <div class="col-sm-9 text-info"><strong>USAGE: </strong><span>Select the IR file to convert.</span>
        </div>
    </div><br>
    <div class="form-group">
        <div class="col-sm-3">
            <div style="position: relative; display: inline-block;">
                <button type="file" class="btn btn-primary">Select IR to convert</button>
                <input
                    style="-webkit-appearance:none; position:absolute; top:0;left:0;opacity:0;width: 100%;height: 100%;"
                    id="audioFileChooser" type="file" accept="audio/x-wav*" onchange="readFile(this.files);" />
            </div>
        </div>
    </div>
</div>
<hr>
<div id="recording-list"></div>
<script>
    var anchor = document.createElement('a')
    document.body.appendChild(anchor)
    anchor.style = 'display: none'

    function readFile(files) {
        var fileReader = new FileReader();
        fileReader.readAsArrayBuffer(files[0]);
        fileReader.onload = function (e) {
            decodeAudioFile(e.target.result, files[0].name);
            console.log(("Filename: '" + files[0].name + "'"), ("(" + ((Math.floor(files[0].size / 1024 / 1024 * 100)) / 100) + " MB)"));
        }
    }
    function decodeAudioFile(file, name) {
        var context = new window.AudioContext();
        context.sampleRate
        context.decodeAudioData(file, function (buffer) {

            var TARGET_SAMPLE_RATE = 48000;

            var offlineCtx = new OfflineAudioContext(buffer.numberOfChannels, 0.06 * TARGET_SAMPLE_RATE, TARGET_SAMPLE_RATE);

            var offlineSource = offlineCtx.createBufferSource();
            offlineSource.buffer = buffer;
            offlineSource.connect(offlineCtx.destination);
            offlineSource.start();
            offlineCtx.startRendering().then((resampled) => {
                var wav = audioBufferToWav(resampled)
                var blob = new window.Blob([new DataView(wav)], {
                    type: 'audio/wav'
                })

                var url = window.URL.createObjectURL(blob)
                anchor.href = url
                anchor.download = name.substr(0, name.lastIndexOf(".")) + '_CV.wav'
                anchor.click()
                window.URL.revokeObjectURL(url)

                //var source = context.createBufferSource();
                //source.buffer = resampled;
                //source.loop = false;
                //source.connect(context.destination);
                //source.start(0);

            });
        });
    }

    function audioBufferToWav(buffer, opt) {
        opt = opt || {}
        var numChannels = 1
        var sampleRate = buffer.sampleRate
        var format = 3;
        var bitDepth = 32;
        var result = buffer.getChannelData(0);
        return encodeWAV(result, format, sampleRate, numChannels, bitDepth)
    }

    function encodeWAV(samples, format, sampleRate, numChannels, bitDepth) {
        var bytesPerSample = bitDepth / 8
        var blockAlign = numChannels * bytesPerSample

        var buffer = new ArrayBuffer(44 + samples.length * bytesPerSample)
        var view = new DataView(buffer)

        /* RIFF identifier */
        writeString(view, 0, 'RIFF')
        /* RIFF chunk length */
        view.setUint32(4, 36 + samples.length * bytesPerSample, true)
        /* RIFF type */
        writeString(view, 8, 'WAVE')
        /* format chunk identifier */
        writeString(view, 12, 'fmt ')
        /* format chunk length */
        view.setUint32(16, 16, true)
        /* sample format (raw) */
        view.setUint16(20, format, true)
        /* channel count */
        view.setUint16(22, numChannels, true)
        /* sample rate */
        view.setUint32(24, sampleRate, true)
        /* byte rate (sample rate * block align) */
        view.setUint32(28, sampleRate * blockAlign, true)
        /* block align (channel count * bytes per sample) */
        view.setUint16(32, blockAlign, true)
        /* bits per sample */
        view.setUint16(34, bitDepth, true)
        /* data chunk identifier */
        writeString(view, 36, 'data')
        /* data chunk length */
        view.setUint32(40, samples.length * bytesPerSample, true)
        if (format === 1) { // Raw PCM
            floatTo16BitPCM(view, 44, samples)
        } else {
            writeFloat32(view, 44, samples)
        }

        return buffer
    }

    function writeFloat32(output, offset, input) {
        for (var i = 0; i < input.length; i++, offset += 4) {
            output.setFloat32(offset, input[i], true)
        }
    }

    function floatTo16BitPCM(output, offset, input) {
        for (var i = 0; i < input.length; i++, offset += 2) {
            var s = Math.max(-1, Math.min(1, input[i]))
            output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true)
        }
    }

    function writeString(view, offset, string) {
        for (var i = 0; i < string.length; i++) {
            view.setUint8(offset + i, string.charCodeAt(i))
        }
    }
</script>